require 'rails_helper'

RSpec.describe Potepan::OrdersController, type: :controller do
  subject { response }

  describe "#edit" do
    let!(:order) { create(:order, line_items: [line_item], guest_token: token) }
    let!(:line_item) { create(:line_item) }
    let(:token) { "sample" }

    before do
      cookies.signed[:guest_token] = token
      get :edit
    end

    describe "レスポンス" do
      it { is_expected.to be_successful }
      it { is_expected.to render_template(:edit) }
    end

    describe "インスタンス変数" do
      it "@order" do
        expect(assigns(:order)).to eq(order)
      end

      it "@line_items" do
        expect(assigns(:line_items)).to eq [line_item]
      end
    end
  end

  describe "#populate" do
    let!(:user) { create(:user) }
    let!(:order) { create(:order) }
    let!(:variant) { create(:variant) }
    let(:params) { post :populate, params: { variant_id: variant.id, quantity: 1 } }

    before { allow(controller).to receive_messages(try_spree_current_user: user) }

    describe 'レスポンス' do
      before { params }

      it { is_expected.to redirect_to(potepan_cart_path) }
    end

    describe "orders" do
      context "current_orderがnilの時" do
        it "orderが作成される" do
          expect { params } .to change(user.orders, :count).by(1)
          expect(assigns(:order)).to eq user.orders.last
        end
      end

      context "current_orderがある時" do
        before { allow(controller).to receive_messages(current_order: order) }

        it "新たにorderが作成されない" do
          expect { params }.not_to change(user.orders, :count)
          expect(assigns(:order)).to eq(order)
        end
      end
    end

    describe "line_items" do
      it "line_itemが作成される" do
        expect { params } .to change(Spree::LineItem, :count).by(1)
        item = Spree::LineItem.last
        expect(item.variant).to eq variant
        expect(item.quantity).to eq 1
      end
    end

    describe "params[:quantity]" do
      context ":quantityが100を超える時" do
        before { post :populate, params: { variant_id: variant.id, quantity: 101 } }

        it "エラーメッセージが表示される" do
          expect(flash[:error]).to be_present
        end

        it "トップページに遷移する" do
          is_expected.to redirect_to potepan_root_path
        end
      end
    end
  end

  describe "#update" do
    context "current_orderがnilの時" do
      before do
        allow(controller).to receive_messages(current_order: nil)
        patch :update
      end

      it "トップページに遷移する" do
        expect(flash[:error]).to be_present
        is_expected.to redirect_to potepan_root_path
      end
    end

    context "current_orderがある時" do
      let(:token) { "sample" }
      let!(:order) { create(:order_with_line_items, state: "cart", guest_token: token) }
      let(:line_item) { order.line_items.first }

      let(:update_params) { patch :update, params: { order: { line_items_attributes: { id: line_item.id, quantity: 2 } } } }
      let(:checkout_params) do
        patch :update, params: {
          checkout: "",
          order: { line_items_attributes: { id: line_item.id, quantity: 2 } },
        }
      end

      before do
        cookies.signed[:guest_token] = token
        allow(controller).to receive_messages(current_order: order)
      end

      context "params[:checkout]がない時" do
        it "orderが更新され、カートページに遷移する" do
          expect { update_params }.to change(line_item, :quantity).from(1).to(2)
          is_expected.to redirect_to potepan_cart_path
        end
      end

      context "params[:checkout]がある時" do
        it "orderのstateが’cart’から’address’に変わる" do
          expect { checkout_params }.to change(order, :state).from("cart").to("address")
          is_expected.to redirect_to potepan_checkout_state_path(order.state)
        end
      end
    end
  end
end
