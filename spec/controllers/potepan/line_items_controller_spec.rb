require 'rails_helper'

RSpec.describe Potepan::LineItemsController, type: :controller do
  describe '#destroy' do
    let!(:line_item) { create(:line_item) }
    let(:delete_params) { delete :destroy, params: { id: line_item.id } }

    it "line_itemを削除する" do
      expect { delete_params }.to change(Spree::LineItem, :count).by(-1)
    end

    it "カートページに遷移する" do
      delete_params
      expect(response).to redirect_to potepan_cart_path
    end
  end
end
