require "rails_helper"

RSpec.describe Potepan::OrderMailer, type: :mailer do
  describe "confirm_email" do
    let(:mail) { Potepan::OrderMailer.confirm_email(order) }
    let(:order)   { create(:completed_order_with_totals) }
    let(:store)   { order.store }

    it "件名が正常である" do
      expect(mail.subject).to eq("注文完了！！ 注文番号[#{order.number}]")
    end

    it "送信先が正常である" do
      expect(mail.to).to eq([order.email])
    end

    it "送信元が正常である" do
      expect(mail.from).to eq([store.mail_from_address])
    end
  end
end
