require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title" do
    context "引数が空のとき" do
      it "'BIGBAG Store'と返す" do
        expect(full_title('')).to eq "BIGBAG Store"
      end
    end

    context "引数が与えられた時" do
      it "'BIGBAG Store'の前に与えられた引数を追加して返す" do
        expect(full_title('Test')).to eq "Test | BIGBAG Store"
      end
    end
  end

  describe "#option_products_count" do
    let!(:taxon)   { create(:taxon) }
    let!(:value)   { create(:option_value) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:variant) { create(:variant, product: product, option_values: [value]) }

    it "option_valueとtaxonに紐付く商品の数を返す" do
      expect(option_products_count(value: value, category: taxon)).to eq 1
    end
  end
end
