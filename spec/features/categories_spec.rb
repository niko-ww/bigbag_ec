require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  include ApplicationHelper
  subject { page }

  let!(:taxonomy)    { create(:taxonomy, name: "Categories") }
  let!(:taxon_bags)  { create(:taxon, name: "bags", taxonomy: taxonomy, products: products) }
  let!(:taxon_other) { create(:taxon, taxonomy: taxonomy) }
  let!(:product_bags) { create(:product, taxons: [taxon_bags]) }
  let!(:products) { [*create_list(:product, 2), red_product, s_product] }
  let(:other_product) { create(:product, taxons: [taxon_other]) }
  let!(:red_product)   { create(:product, variants: [color_variant]) }
  let!(:color_variant) { create(:variant, option_values: [color_value]) }
  let!(:color_value)   { create(:option_value, presentation: "Red", option_type: color_type) }
  let!(:color_type)    { create(:option_type, presentation: "Color") }
  let!(:s_product)    { create(:product, variants: [size_variant]) }
  let!(:size_variant) { create(:variant, option_values: [size_value]) }
  let!(:size_value)   { create(:option_value, presentation: "Small", option_type: size_type) }
  let!(:size_type)    { create(:option_type, presentation: "Size") }

  before { visit potepan_category_path(taxon_bags.id) }

  scenario 'サイドバーが表示される' do
    click_on taxonomy.name

    within(".col-md-3") do
      is_expected.to have_link taxon_bags.name, href: potepan_category_path(taxon_bags.id)
      is_expected.to have_content(taxon_bags.products.count)
      is_expected.to have_link taxon_other.name, href: potepan_category_path(taxon_other.id)
      is_expected.to have_link color_value.presentation,
                               href: potepan_category_path(taxon_bags.id, value: color_value.presentation)
      is_expected.to have_content([red_product].count)
      is_expected.to have_link size_value.presentation,
                               href: potepan_category_path(taxon_bags.id, value: size_value.presentation)
      is_expected.to have_content([s_product].count)
    end
  end

  scenario 'ユーザーが商品カテゴリー(Bags)をクリックすると、商品が表示される' do
    visit potepan_category_path(taxon_other.id)
    click_on taxonomy.name
    click_on taxon_bags.name

    is_expected.to have_title("#{taxon_bags.name} | BIGBAG Store")
    within("#row_productbox") do
      is_expected.to have_link product_bags.name, href: potepan_product_path(product_bags.id)
      is_expected.to have_link product_bags.display_price, href: potepan_product_path(product_bags.id)
      is_expected.not_to have_content(other_product.name)
    end
  end

  scenario "ユーザーが色から探す（red）をクリックすると、商品が表示される" do
    click_on color_value.presentation

    within("#row_productbox") do
      is_expected.to have_link red_product.name, href: potepan_product_path(red_product.id)
      is_expected.to have_link red_product.display_price, href: potepan_product_path(red_product.id)
      is_expected.not_to have_content(product_bags.name, s_product.name)
    end
  end

  scenario "ユーザーがサイズから探す(S)をクリックすると、商品が表示される" do
    click_on size_value.presentation

    within("#row_productbox") do
      is_expected.to have_link s_product.name, href: potepan_product_path(s_product.id)
      is_expected.to have_link s_product.display_price, href: potepan_product_path(s_product.id)
      is_expected.not_to have_content(red_product.name, other_product.name)
    end
  end

  scenario 'ユーザーが商品一覧から商品をクリックすると、詳細ページが表示される' do
    click_on product_bags.name

    expect(current_path).to eq(potepan_product_path(product_bags.id))
    within("#media_body") do
      is_expected.to have_content(product_bags.name)
      is_expected.to have_content(product_bags.display_price)
      is_expected.to have_content(product_bags.description)
    end
  end

  scenario '商品を並び替えるセレクトボックスが表示される' do
    within(".filterArea") do
      is_expected.to have_select(
        'sort',
        selected: "新着順",
        options: ["新着順", "価格の高い順", "価格の安い順", "古い順"]
      )
    end
  end

  scenario 'セレクトボックス・価格の高い順をクリックすると、セレクトボックスが切り替わる' do
    within(".filterArea") do
      select("価格の高い順", from: "sort")
    end

    is_expected.to have_select("sort", selected: "価格の高い順")
  end
end
