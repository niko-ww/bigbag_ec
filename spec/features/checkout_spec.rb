require 'rails_helper'

RSpec.feature "Checkout", type: :feature do
  include ApplicationHelper
  subject { page }

  let(:order)  { user.orders.last }
  let!(:store) { create(:store) }
  let!(:user)  { create(:user) }

  let!(:taxon)   { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }

  let!(:credit_card)     { create(:credit_card_payment_method) }
  let!(:shipping_method) { create(:shipping_method) }

  before do
    allow_any_instance_of(Potepan::OrdersController).to receive_messages(try_spree_current_user: user)
    allow_any_instance_of(Potepan::CheckoutController).to receive_messages(try_spree_current_user: user)
  end

  scenario "ユーザーが商品を購入する" do
    go_to_checkout

    expect(current_path).to eq potepan_checkout_state_path("address")
    is_expected.to have_title full_title("Checkout")

    # 無効なadressを入力
    fill_in_invalid_address
    click_on "次へ"

    is_expected.to have_content("The form contains 5 errors.")

    fill_in_address
    click_on "次へ"

    expect(current_path).to eq potepan_checkout_state_path("payment")

    # 無効なクレジットカード情報を入力
    fill_in_invalid_credit_card
    click_on "次へ"

    is_expected.to have_content("The form contains 3 errors.")

    # Bogus Gateway: Forced failure エラーを発生させる
    fill_in_credit_card("123")
    click_on "次へ"
    click_on "購入確定"

    is_expected.to have_content("Bogus Gateway: Forced failure")

    fill_in_credit_card("4111111111111111")
    click_on "次へ"

    expect(current_path).to eq potepan_checkout_state_path("confirm")
    within ".confirm-address" do
      is_expected.to have_content("test 123")
      is_expected.to have_content("〒 1234567")
      is_expected.to have_content("test")
      is_expected.to have_content("123")
      is_expected.to have_content("09012345678")
      is_expected.to have_content("admin@example.com")
    end
    within ".totalAmountArea" do
      is_expected.to have_content(order.display_item_total)
      is_expected.to have_content(order.display_tax_total)
      is_expected.to have_content(order.display_ship_total)
      is_expected.to have_content(order.display_total)
    end

    click_on "購入確定"

    expect(current_path).to eq potepan_order_path(order.number)
    is_expected.to have_title full_title("Order Complete")
    is_expected.to have_content("ご注文ありがとうございます。")
    is_expected.to have_content(order.number)
  end

  def go_to_checkout
    visit potepan_product_path(product.id)
    select "1", from: "quantity"
    click_button "カートへ入れる"
    click_on "購入する"
  end

  def fill_in_invalid_address
    fill_in "order_email", with: ""
    address = "order_ship_address_attributes"
    fill_in "#{address}_last_name",  with: ""
    fill_in "#{address}_first_name", with: ""
    fill_in "#{address}_phone",     with: ""
    fill_in "#{address}_zipcode",   with: ""
    fill_in "#{address}_city",      with: ""
    fill_in "#{address}_address1",  with: ""
    select "Alabama", from: "#{address}_state_id"
  end

  def fill_in_address
    fill_in 'order_email', with: 'admin@example.com'
    address = "order_ship_address_attributes"
    fill_in "#{address}_last_name",  with: "123"
    fill_in "#{address}_first_name", with: "test"
    fill_in "#{address}_phone",     with: "09012345678"
    fill_in "#{address}_zipcode",   with: "1234567"
    fill_in "#{address}_city",      with: "test"
    fill_in "#{address}_address1",  with: "123"
    select "Alabama", from: "#{address}_state_id"
  end

  def fill_in_invalid_credit_card
    payment = "payment_source_#{credit_card.id}"
    fill_in "#{payment}_name", with: ""
    fill_in "#{payment}_number", with: ""
    fill_in "#{payment}_verification_value", with: ""
  end

  def fill_in_credit_card(number)
    payment = "payment_source_#{credit_card.id}"
    fill_in "#{payment}_name", with: 'test'
    fill_in "#{payment}_number", with: number
    select "1", from: "payment_source_#{credit_card.id}_month"
    select "#{Time.current.year}", from: "payment_source_#{credit_card.id}_year"
    fill_in "#{payment}_verification_value", with: "123"
  end
end
