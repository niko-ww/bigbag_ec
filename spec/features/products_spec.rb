require 'rails_helper'

RSpec.feature "Products", type: :feature do
  subject { page }

  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }
  let(:not_related_product) { create(:product) }

  before { visit potepan_product_path(product.id) }

  scenario "商品個別ページが表示される" do
    is_expected.to have_title("#{product.name} | BIGBAG Store")
    is_expected.to have_selector 'h2', text: product.name
    is_expected.to have_selector 'h3', text: product.display_price
    is_expected.to have_selector 'p', text: product.description
    is_expected.to have_link '一覧ページへ戻る', href: potepan_category_path(product.taxons.first.id)
  end

  scenario '商品個別ページに関連商品が表示される' do
    within(".productsContent") do
      related_products[0..3].each do |related|
        is_expected.to have_content(related.name)
        is_expected.to have_content(related.display_price)
      end
      is_expected.not_to have_content(not_related_product.name)
      is_expected.not_to have_content(product.name)
    end
  end

  scenario "ユーザーが関連商品をクリックすると、商品詳細ページが表示される" do
    click_on related_products[0].name

    expect(current_path).to eq(potepan_product_path(related_products[0].id))
    is_expected.to have_selector 'h2', text: related_products[0].name
    is_expected.to have_selector 'h3', text: related_products[0].display_price
    is_expected.to have_selector 'p', text: related_products[0].description
  end
end
