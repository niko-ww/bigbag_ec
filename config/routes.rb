Rails.application.routes.draw do
  mount Spree::Core::Engine, at: '/'

  namespace :potepan do
    root  'home_pages#index'

    get   :cart,     to: "orders#edit"
    patch :cart,     to: "orders#update"
    post  :populate, to: "orders#populate"

    patch "/checkout/update/:state", to: "checkout#update", as: :update_checkout
    get   "/checkout/:state",        to: "checkout#edit",   as: :checkout_state
    get   "/checkout",               to: "checkout#edit"

    resources :orders,              only: [:show, :edit, :update]
    resources :line_items,          only: [:destroy]
    resources :home_pages,          only: [:index]
    resources :categories,          only: [:show]
    resources :products,            only: [:show] do
      collection do
        get :search
      end
    end

    get :product_list_left_sidebar, to: 'sample#product_list_left_sidebar'
    get :about_us,                  to: 'sample#about_us'
    get :tokushoho,                 to: 'sample#tokushoho'
    get :privacy_policy,            to: 'sample#privacy_policy'
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
