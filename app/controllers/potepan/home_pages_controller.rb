class Potepan::HomePagesController < ApplicationController
  NEW_PRODUCT_MAX_NUM = 8

  def index
    @new_release_products = Spree::Product.desc_order_available.limit(NEW_PRODUCT_MAX_NUM)
    @categories = Spree::Taxon.where(name: %w(bags t-shirts mugs))
  end
end
