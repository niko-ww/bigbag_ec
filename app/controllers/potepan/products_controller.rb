class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCT_MAX_NUM = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products(RELATED_PRODUCT_MAX_NUM)
  end

  def search
    if params[:search]
      @search_keyword = params[:search]
      @search_products = Spree::Product.in_name_or_description(@search_keyword)
    end
  end
end
