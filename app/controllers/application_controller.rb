class ApplicationController < ActionController::Base
  include Spree::Core::ControllerHelpers::Order
  include Spree::Core::ControllerHelpers::Store
  include Spree::Core::ControllerHelpers::Auth
  include Spree::Core::ControllerHelpers::StrongParameters
  include Spree::Core::ControllerHelpers::PaymentParameters

  protect_from_forgery with: :exception
end
