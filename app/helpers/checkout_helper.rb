module CheckoutHelper
  def section_title(order)
    case order.state
    when "address"
      "お届け先情報"
    when "payment"
      "お支払い方法"
    when "confirm"
      "入力内容確認"
    end
  end
end
