module Spree::OrderDecorator
  def previous_state
    case state
    when "address"
      "cart"
    when "payment"
      "address"
    when "confirm"
      "payment"
    end
  end

  def step_design
    case state
    when "address"
      "disabled"
    when "payment"
      "active"
    when "confirm"
      "complete"
    end
  end

  def payment_by(methods)
    available_payment_methods.find_by(name: methods)
  end

  Spree::Order.prepend self
end
